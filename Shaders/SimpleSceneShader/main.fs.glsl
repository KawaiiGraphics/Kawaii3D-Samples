module FragmentMain;
precision mediump int; precision highp float;

layout(location = 0) in highp vec3 texcoord;
layout(location = 0) out highp vec4 color;

import getTexel, CurrentFragGlobalStruct from TextureHelper;

CurrentFragGlobalStruct currentFragGlobal;

//import NotFound from TextureHelper;

void main(void)
{
    getTexel(texcoord.xy);
    color = vec4(currentFragGlobal.color, 1.0);
}
