module VertexMain;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in highp vec4 qt_Vertex;
layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in highp vec3 texcoord;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

layout(location = 0) out highp vec3 texCoord;

void main(void)
{
    gl_Position = surface.vp_mat * qt_Vertex;
    texCoord = vec3(texcoord);
}
