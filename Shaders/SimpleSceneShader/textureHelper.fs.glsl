module TextureHelper;
precision mediump int; precision highp float;

Material
{
    vec4 bg;
} material;

Model {
    sampler2D tex;
} model;

export getTexel, CurrentFragGlobalStruct;

Global_1
{
    vec2 texCoordCenter;
    sampler2D bgTexture;
} global_1;

struct CurrentFragGlobalStruct
{
    vec3 color;
};

CurrentFragGlobalStruct currentFragGlobal = CurrentFragGlobalStruct(vec3(0.75, 0.5, 0.5));

void getTexel(in vec2 texCoord)
{
    if(global_1.texCoordCenter != vec2(0.5, 0.5))
        return;

    float k = 1.0 - distance(texCoord, vec2(0.5, 0.5));

    vec4 texelColor = texture(tex, texCoord);
    vec4 bgColor = texture(bgTexture, texCoord);
    bgColor.rgb = pow(mix(material.bg.rgb, bgColor.rgb, bgColor.a), vec3(2.2));

    currentFragGlobal.color = k * mix(bgColor.rgb, texelColor.rgb, texelColor.a);
}
