module FragmentMain;
precision mediump int; precision highp float;

layout(location = 1) in highp vec3 pos;
layout(location = 0) out highp vec4 color;

void main(void)
{
    vec2 texc = (vec3(0.5) + pos * 0.5).xy;
    color = vec4(0.4 * cos(36.0 * texc) + vec2(0.7),
                 0.4 * sin(36.0 * texc.x * texc.y) + 0.7,
                 1.0);
}
