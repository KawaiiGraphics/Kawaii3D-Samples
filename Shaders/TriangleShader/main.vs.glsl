module VertexMain;
precision mediump int; precision highp float;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
//layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;
//layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

layout(location = 1) out highp vec3 pos;

void main(void)
{
    gl_Position = surface.vp_mat * qt_Vertex;
    pos = vec3(qt_Vertex);
}
