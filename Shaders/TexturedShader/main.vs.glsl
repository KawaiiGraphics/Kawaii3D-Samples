module VertexMainTex;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;
layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in vec3 texcoord;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

layout(location = 0) out vec2 texCoord;
layout(location = 1) out vec3 norm_vec;
layout(location = 2) out vec3 pos_vec;

void main(void)
{
    texCoord = vec2(texcoord.x, texcoord.y);
    norm_vec = qt_Normal;
    pos_vec = qt_Vertex.xyz;
    gl_Position = surface.vp_mat * qt_Vertex;
}
