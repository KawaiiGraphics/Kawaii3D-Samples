module FragmentMain;

layout(location = 0) in vec2 texCoord;
layout(location = 1) in vec3 norm_vec;
layout(location = 2) in vec3 pos_vec;

Material
{
    sampler2D tex;
} material;

import get_illumination from PBR_illumination;
import MaterialInfo from PbrIlluminationCore;

//albedo; metallic; roughness; ao;
MaterialInfo pbr_material;

Global_3
{
    float exposure;
    float gamma;
} global_3;

Camera
{
    vec4 camPos;
    mat4 view_mat;
    mat4 viewInverted;
    mat3 normal;
} camera;

layout(location = 0) out vec4 result;

void main()
{
    pbr_material.metallic = 0.05;
    pbr_material.roughness = 0.5;
    pbr_material.ao = 1.0;

    vec4 texel = texture(tex, texCoord);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    pbr_material.albedo = pow(texel.rgb, vec3(2.2));
    vec3 c = get_illumination(norm_vec, pos_vec, camera.camPos.xyz);
    vec3 mapped = vec3(1.0) - exp(-c * global_3.exposure);
    mapped = pow(mapped, vec3(1.0 / global_3.gamma));
    result = vec4(mapped, texel.a);
}
