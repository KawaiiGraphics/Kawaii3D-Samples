module VertexMainGray;

layout(location = ${SIB_VERTEX_ATTR_LOCATION}) in vec4 qt_Vertex;
layout(location = ${SIB_NORMAL_ATTR_LOCATION}) in vec3 qt_Normal;

Surface {
    mat4 proj_mat;
    mat4 vp_mat;
} surface;

layout(location = 0) out vec3 norm_vec;
layout(location = 1) out vec3 pos_vec;

void main(void)
{
    norm_vec = qt_Normal;
    pos_vec = qt_Vertex.xyz;
    gl_Position = surface.vp_mat * qt_Vertex;
}
