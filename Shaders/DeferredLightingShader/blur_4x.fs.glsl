module FragmentMain;

sampled rgba32f gbuf_0;
layout(location = 0) out vec4 result;

void main()
{
    vec4 val = vec4(0.0);
    for (int x = -2; x < 2; ++x)
        for (int y = -2; y < 2; ++y)
            val += texelFetch(SAMPLED_GBUF_0, ivec2(gl_FragCoord.x+x, gl_FragCoord.y+y), 0);
    result = val / vec4(4.0 * 4.0);
}
