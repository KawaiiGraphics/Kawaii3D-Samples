module FragmentMain;
precision mediump int; precision highp float;

layout(location = 1) in highp vec3 pos;
layout(location = 2) in highp vec3 norm;
//layout(location = 3) in highp vec2 texc;

layout(location = 0) out highp vec4 color;
layout(location = 1) out highp vec3 position;
layout(location = 2) out highp vec3 normal;

void main(void)
{
    color = vec4(1.0, 0.75, 0.6, 1);
//    color = vec4(0.4 * cos(36.0 * texc) + vec2(0.7),
//                 0.4 * sin(36.0 * texc.x * texc.y) + 0.7,
//                 1.0);
    position = pos;
    normal = (gl_FrontFacing)? norm: -norm;
}
