module FragmentMain;

input rgba8 gbuf_0;  // albedo
input rgba16f gbuf_1; // position
input rgba16f gbuf_2; // normal
input r16f gbuf_3; // ao

import get_illumination from Illumination_illumination;
import MaterialInfo from PbrIlluminationCore;

Camera {
    vec4 camPos;
    mat4 viewMat;
    mat4 viewMatInverted;
    mat3 normalMat;
} camera;

//albedo; metallic; roughness; ao;
MaterialInfo pbr_material;

layout(location = 0) out vec4 result;

void main()
{
    vec4 texel = subpassLoad(INPUT_GBUF_0);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }

    pbr_material.metallic = 0.66;
    pbr_material.roughness = 0.5;
    pbr_material.ao = pow(subpassLoad(INPUT_GBUF_3).r, 1.25);

    vec3 pos_vec = subpassLoad(INPUT_GBUF_1).xyz;
    vec3 norm_vec = subpassLoad(INPUT_GBUF_2).xyz;

    pbr_material.albedo = pow(texel.rgb, vec3(2.2));

    vec3 c = get_illumination(norm_vec, pos_vec, camera.camPos.xyz);
    result = vec4(c * pbr_material.ao, 1.0);
}
