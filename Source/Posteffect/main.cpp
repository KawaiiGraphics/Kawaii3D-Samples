﻿#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include <Kawaii3D/Illumination/KawaiiIllumination.hpp>
#include <Kawaii3D/Illumination/KawaiiLampsUbo.hpp>
#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiShaderEffect.hpp>
#include <Kawaii3D/Renderpass/KawaiiTexLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

#include <random>
#include <array>

#include <QCommandLineParser>
#include <QGuiApplication>
#include <QMatrix4x4>
#include <QPainter>
#include <QTimer>

#include <glm/gtc/matrix_transform.hpp>

namespace
{
  float frametime = -1;
  float rendertime = -1;
  float maxFrametime = std::numeric_limits<float>().min();
  float minFrametime = std::numeric_limits<float>().max();
  QString rendererName;
  float t = 0;

  void drawHUD(QPainter &p)
  {
    int yPos = p.window().height() - (15 * 3);
    p.setPen(Qt::green);

    static const QString rendererPluginStr = QStringLiteral("Renderer plugin = %1").arg(rendererName);
    p.drawText(20, yPos, rendererPluginStr);
    yPos += 15;

    if(frametime > 0 && frametime < minFrametime)
      minFrametime = frametime;

    if(frametime > maxFrametime)
      maxFrametime = frametime;

    p.drawText(20, yPos, QStringLiteral("Frametime = %1 ms").arg(frametime, 0, 'f', 3));
    p.drawText(220, yPos, QStringLiteral("[%1; %2]").arg(minFrametime, 0, 'f', 3).arg(maxFrametime, 0, 'f', 3));
    yPos += 15;
    p.drawText(20, yPos, QStringLiteral("Rendertime = %2 ms").arg(rendertime, 0, 'f', 3));
//    t += 0.02*frametime;
  }

  KawaiiLampsUbo<KawaiiPbrLight> *lamps = nullptr;

  void timerTrigger()
  {
    if(lamps)
      {
        t += 0.5;
        QMatrix4x4 mat;
        mat.rotate(t, 0,1,0);
        lamps->getDotLight(0).position = mat.map(QVector3D(0, 0, -2));
        lamps->updateDotLight(0);

        QMatrix4x4 mat2;
        mat2.rotate(15.0 * std::sin(0.15*t), 0,0,1);
        lamps->getDotLight(1).position = mat2.map(QVector3D(0, -2, 0));
        lamps->updateDotLight(1);
        lamps->getDotLight(2).position = mat2.map(QVector3D(0, 2, 0));
        lamps->updateDotLight(2);
      }
  }

  struct SsaoData {
    glm::mat4 projectionMat;
    std::array<glm::vec4, 64> samples;
  };

  float lerp(float a, float b, float f)
  {
      return a + f * (b - a);
  }

  QString appDir;

  uint32_t sceneSubpassIndex;
  uint32_t ssaoSubpassIndex;
  uint32_t ssaoBlurSubpassIndex;
  uint32_t lightingSubpassIndex;
  uint32_t frameSubpassIndex;
  uint32_t hudSubpassIndex;
}

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  QCommandLineParser argParser;
  QCommandLineOption overlay("overlay", "Shows overlay with frametime and other rendering info"),
      no_overlay("no-overlay", "Hide overlay with frametime and other rendering info");

  argParser.addOptions({ overlay, no_overlay });
  auto helpOpt = argParser.addHelpOption();
  argParser.process(app);
  if(argParser.isSet(helpOpt))
    {
      argParser.showHelp();
      return 0;
    }

  appDir = app.applicationDirPath();
  bool overlayEnabled = !argParser.isSet(no_overlay) || argParser.isSet(overlay);

  KawaiiRoot root;
  KawaiiRenderpass *renderpass = root.createChild<KawaiiRenderpass>();
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA16_F, .needClear = false, .needStore = false });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA16_F, .needClear = false, .needStore = false });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::R16_F, .needClear = false, .needStore = false });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::R16_F, .needClear = false, .needStore = false });

  KawaiiMaterial *material = root.createChild<KawaiiMaterial>();
  KawaiiProgram *prog = root.createChild<KawaiiProgram>();
  prog->setObjectName(QStringLiteral("SceneGlsl"));
  prog->createShaderModule(KawaiiShaderType::Vertex, QFile(appDir + "/../share/Shaders/DeferredLightingShader/scene.vs.glsl"));
  prog->createShaderModule(KawaiiShaderType::Fragment, QFile(appDir + "/../share/Shaders/DeferredLightingShader/scene.fs.glsl"));
  QMatrix4x4 trMat0;
  trMat0.translate(0.6, 0, -2);
  KawaiiMesh3D *mesh0 = root.createChild<KawaiiMesh3D>();
  mesh0->loadAsset(QStringLiteral(":/sphere_x0.4"), trMat0);
  mesh0->setObjectName(QStringLiteral("sphere"));
  mesh0->createChild<KawaiiMeshInstance>(material);
  KawaiiMesh3D *mesh1 = root.createChild<KawaiiMesh3D>();
  QMatrix4x4 trMat1;
  trMat1.translate(0, -0.4, -2);
  trMat1.scale(0.75);
  mesh1->loadAssetP(appDir + QStringLiteral("/../share/Models/human.dae"), trMat1);
  mesh1->setObjectName(QStringLiteral("Human"));
  mesh1->createChild<KawaiiMeshInstance>(material);
  QMatrix4x4 trMat2;
  trMat2.rotate(45, 0,1,0);
  KawaiiMesh3D *mesh2 = root.createChild<KawaiiMesh3D>();
  mesh2->loadAssetP(QStringLiteral(":/cube_x2.2"), trMat2);
  mesh2->setObjectName(QStringLiteral("cube"));
  mesh2->createChild<KawaiiMeshInstance>(material);
  KawaiiScene *sc = root.createChild<KawaiiScene>();
  sc->setProgramToMaterial(material, prog);
  KawaiiCamera *cam = sc->createChild<KawaiiCamera>();
  cam->setViewMat(glm::lookAt(glm::vec3(0,0,0), glm::vec3(0,0,-1), glm::vec3(0,1,0)));
  KawaiiSceneLayer *sceneLayer = renderpass->createChild<KawaiiSceneLayer>();
  sceneLayer->setCamera(cam);
  sceneSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = sceneLayer, .outputGBufs = {0,1,2}, .depthGBuf = 3 } );

  KawaiiProgram *ssaoProg = root.createChild<KawaiiProgram>();
  ssaoProg->setObjectName(QStringLiteral("SSAOGlsl"));
  ssaoProg->setCullMode(KawaiiProgram::CullMode::CullBack);
  ssaoProg->createShaderModule(KawaiiShaderType::Fragment, QFile(appDir + "/../share/Shaders/DeferredLightingShader/ssao.fs.glsl"));
  KawaiiShaderEffect *ssaoShaderEffect = renderpass->createChild<KawaiiShaderEffect>(ssaoProg);
  ssaoShaderEffect->setDepthTest(false);
  ssaoShaderEffect->setDepthWrite(false);
  KawaiiGpuBuf *ssaoBuf = root.createChild<KawaiiGpuBuf>(nullptr, sizeof(SsaoData));
  ssaoBuf->addAlias(sceneLayer->getSurfaceUBO(),
                    offsetof(KawaiiSceneLayer::SurfaceGpuData, projectionMat),
                    offsetof(SsaoData, projectionMat),
                    sizeof(SsaoData::projectionMat));
  std::uniform_real_distribution<float> randomFloats(0.0, 1.0);
  std::default_random_engine generator(time(0));
  auto *ssaoData = static_cast<SsaoData*>(ssaoBuf->getData());
  for(size_t i = 0; i < ssaoData->samples.size(); ++i)
    {
      glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
      sample = glm::normalize(sample);
      sample *= randomFloats(generator);
      float scale = float(i) / 64.0f;

      // scale samples s.t. they're more aligned to center of kernel
      scale = lerp(0.1f, 1.0f, scale * scale);
      sample *= scale;
      ssaoData->samples[i] = glm::vec4(sample, 0.0);
    }
  KawaiiImage *texNoise = ssaoBuf->createChild<KawaiiImage>();
  texNoise->setImage(QImage(4,4, QImage::Format_ARGB32));
  texNoise->changeImage([&generator] (QImage &img) {
    std::uniform_int_distribution<int> randomInts(0, 255);
    for(int x = 0; x < 4; ++x)
      for(int y = 0; y < 4; ++y)
        img.setPixelColor(x,y, QColor(randomInts(generator), randomInts(generator), 128));
    return true;
  });
  ssaoBuf->bindTexture(QStringLiteral("texNoise"), texNoise);
  ssaoShaderEffect->setSurfaceUBO(ssaoBuf);
  ssaoShaderEffect->setCamera(cam);
  ssaoSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = ssaoShaderEffect, .inputGBufs = {2}, .outputGBufs = {4}, .sampledGBufs = {1}, .depthGBuf = std::nullopt } );
  renderpass->addNonLocalDependency(sceneSubpassIndex, ssaoSubpassIndex);

  KawaiiProgram *blurProg = root.createChild<KawaiiProgram>();
  blurProg->setObjectName(QStringLiteral("Blur4x_Glsl"));
  blurProg->createShaderModule(KawaiiShaderType::Fragment, QFile(appDir + "/../share/Shaders/DeferredLightingShader/blur_4x.fs.glsl"));
  blurProg->setCullMode(KawaiiProgram::CullMode::CullBack);
  KawaiiShaderEffect *blurShaderEffect = renderpass->createChild<KawaiiShaderEffect>(blurProg);
  blurShaderEffect->setDepthTest(false);
  blurShaderEffect->setDepthWrite(false);
  ssaoBlurSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = blurShaderEffect, .outputGBufs = {5}, .sampledGBufs = {4}, .depthGBuf = std::nullopt } );
  renderpass->addNonLocalDependency(ssaoSubpassIndex, ssaoBlurSubpassIndex);

  KawaiiProgram *lightingProg = root.createChild<KawaiiProgram>();
  lightingProg->setObjectName(QStringLiteral("LightingGlsl"));
  lightingProg->createShaderModule(KawaiiShaderType::Fragment, QFile(appDir + "/../share/Shaders/DeferredLightingShader/lighting.fs.glsl"));
  lightingProg->setCullMode(KawaiiProgram::CullMode::CullBack);
  KawaiiShaderEffect *lightingShaderEffect = renderpass->createChild<KawaiiShaderEffect>(lightingProg);
  KawaiiIllumination *illumination = lightingShaderEffect->createChild<KawaiiIllumination>(QStringLiteral("Illumination"));
  illumination->createLampArrays<KawaiiPbrLight>();
  lamps = static_cast<KawaiiLampsUbo<KawaiiPbrLight>*>(&illumination->getLampsLayout());
  lamps->addDotLight();
  lamps->getDotLight(0).ambient = QVector3D(0.5, 0.5, 0.5);
  lamps->getDotLight(0).constAttenuation = 0;
  lamps->getDotLight(0).linearAttenuation = 0;
  lamps->getDotLight(0).quadraticAttenuation = 0.5;
  lamps->getDotLight(0).position = QVector3D(0, 0, -2);
  static_cast<KawaiiIlluminationStructs<KawaiiPbrLight>::Lamp&>(lamps->getDotLight(0)).color = QVector3D(1,1,1);
  lamps->updateDotLight(0);

  lamps->addDotLight();
  lamps->getDotLight(1).ambient = QVector3D(0, 0, 0);
  lamps->getDotLight(1).constAttenuation = 0;
  lamps->getDotLight(1).linearAttenuation = 0;
  lamps->getDotLight(1).quadraticAttenuation = 0.5;
  lamps->getDotLight(1).position = QVector3D(0,-2, 0);
  static_cast<KawaiiIlluminationStructs<KawaiiPbrLight>::Lamp&>(lamps->getDotLight(1)).color = QVector3D(1,1,1);
  lamps->updateDotLight(1);

  lamps->addDotLight();
  lamps->getDotLight(2).ambient = QVector3D(0, 0, 0);
  lamps->getDotLight(2).constAttenuation = 0;
  lamps->getDotLight(2).linearAttenuation = 0;
  lamps->getDotLight(2).quadraticAttenuation = 0.5;
  lamps->getDotLight(2).position = QVector3D(0, 2, 0);
  static_cast<KawaiiIlluminationStructs<KawaiiPbrLight>::Lamp&>(lamps->getDotLight(2)).color = QVector3D(1,1,1);
  lamps->updateDotLight(2);
  QTimer timer;
  QObject::connect(&timer, &QTimer::timeout, &timerTrigger);
  timer.start(20);
  timer.setSingleShot(false);

  KawaiiGpuBuf *lampsBuf = illumination->createChild<KawaiiGpuBuf>();
  lampsBuf->setObjectName(QStringLiteral("LampsBuf"));
  illumination->setGpuBuffer(lampsBuf, 0, QStringLiteral("Surface\n"
                                                         "{\n"
                                                         "} surface;\n"));
  lightingShaderEffect->setSurfaceUBO(lampsBuf);
  lightingShaderEffect->setCamera(cam);
  lightingShaderEffect->setDepthTest(false);
  lightingShaderEffect->setDepthWrite(false);
  illumination->tryEnable();
  lightingSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = lightingShaderEffect, .inputGBufs = {0,1,2,5}, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
  renderpass->addLocalDependency(ssaoBlurSubpassIndex, lightingSubpassIndex);

  KawaiiImage *frameImg = root.createChild<KawaiiImage>();
  frameImg->setObjectName(QStringLiteral("Frame"));
  frameImg->setMagFilter(KawaiiTextureFilter::Nearest);
  frameImg->setMinFilter(KawaiiTextureFilter::Linear);
  frameImg->changeImage([] (QImage &img) {
    return img.load(appDir + QStringLiteral("/../share/Pictures/frame.png"));
  });
  KawaiiTexLayer *texLayer = renderpass->createChild<KawaiiTexLayer>(frameImg);
  frameSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = texLayer, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
  renderpass->addLocalDependency(lightingSubpassIndex, frameSubpassIndex);

  KawaiiSurface *sfc = root.createChild<KawaiiSurface>(renderpass);
  sfc->resize(QSize(1280, 768));
  if(overlayEnabled)
    {
      sfc->setFrametimeCounter(&frametime);
      sfc->setRendertimeCounter(&rendertime);
      auto hudLayer = renderpass->createChild<KawaiiQPainterLayer>(nullptr, &drawHUD);
      hudSubpassIndex = renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = hudLayer, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
      renderpass->addLocalDependency(frameSubpassIndex, hudSubpassIndex);

      QTimer *minMaxTimesRefresher = new QTimer(&root);
      QObject::connect(minMaxTimesRefresher, &QTimer::timeout, [] {
          maxFrametime = std::numeric_limits<float>().min();
          minFrametime = std::numeric_limits<float>().max();
        });
      minMaxTimesRefresher->start(1000);
      minMaxTimesRefresher->setSingleShot(false);
    }

  sfc->showMaximized();
  root.createRendererImplementation();

  root.dumpObjectTree();
  qDebug("=====================");
  rendererName = KawaiiRender::getPreferredRender().getPluginName();
  qDebug("Selected renderer plugin \"%s\"", rendererName.toLatin1().data());

  return app.exec();
}
