﻿#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QPainter>

namespace
{
  float frametime = -1;
  float rendertime = -1;
  float maxFrametime = std::numeric_limits<float>().min();
  float minFrametime = std::numeric_limits<float>().max();
  QString rendererName;

  void drawHUD(QPainter &p)
  {
    int yPos = p.window().height() - (15 * 3);
    p.setPen(Qt::white);

    static const QString rendererPluginStr = QString("Renderer plugin = %1").arg(rendererName);
    p.drawText(20, yPos, rendererPluginStr);
    yPos += 15;

    if(frametime > 0 && frametime < minFrametime)
      minFrametime = frametime;

    if(frametime > maxFrametime)
      maxFrametime = frametime;

    const auto benchStr = QString("Frametime = %1 ms [%3; %4]\t\tRendertime = %2 ms").arg(frametime, 0, 'f', 3).arg(rendertime, 0, 'f', 3).arg(minFrametime, 0, 'f', 3).arg(maxFrametime, 0, 'f', 3);
    p.drawText(20, yPos, benchStr);
  }
}

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  QCommandLineParser argParser;
  QCommandLineOption overlay("overlay", "Shows overlay with frametime and other rendering info"),
      no_overlay("no-overlay", "Hide overlay with frametime and other rendering info");

  argParser.addOptions({ overlay, no_overlay });
  auto helpOpt = argParser.addHelpOption();
  argParser.process(app);
  if(argParser.isSet(helpOpt))
    {
      argParser.showHelp();
      return 0;
    }

  bool overlayEnabled = !argParser.isSet(no_overlay) || argParser.isSet(overlay);

  qDebug("=====================");
  rendererName = KawaiiRender::getPreferredRender().getPluginName();
  qDebug("Selected renderer plugin \"%s\"", rendererName.toLatin1().data());

  KawaiiRoot root;
  KawaiiMaterial *material = root.createChild<KawaiiMaterial>();
  KawaiiProgram *prog = root.createChild<KawaiiProgram>();
  prog->createShaderModule(KawaiiShaderType::Vertex, QFile(app.applicationDirPath() + "/../share/Shaders/TriangleShader/main.vs.glsl"));
  prog->createShaderModule(KawaiiShaderType::Fragment, QFile(app.applicationDirPath() + "/../share/Shaders/TriangleShader/main.fs.glsl"));
  prog->setCullMode(KawaiiProgram::CullMode::CullFront);

  KawaiiMesh3D *mesh = root.createChild<KawaiiMesh3D>();
  mesh->addVertex(QVector4D( 1, -1, 5,1));
  mesh->addVertex(QVector4D( 0,  1, 5,1));
  mesh->addVertex(QVector4D(-1, -1, 5,1));
  mesh->addTriangle(0,1,2);
  mesh->createChild<KawaiiMeshInstance>(material);

  KawaiiScene *sc = root.createChild<KawaiiScene>();
  sc->setProgramToMaterial(material, prog);

  KawaiiCamera *cam = sc->createChild<KawaiiCamera>();
  KawaiiRenderpass *renderpass = root.createChild<KawaiiRenderpass>();
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  KawaiiSceneLayer *sceneLayer = renderpass->createChild<KawaiiSceneLayer>();
  sceneLayer->setCamera(cam);
  sceneLayer->setDepthTest(false);
  sceneLayer->setDepthWrite(false);
  renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = sceneLayer, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
  KawaiiSurface *sfc = root.createChild<KawaiiSurface>(renderpass);
  sfc->resize(QSize(1280, 768));
  if(overlayEnabled)
    {
      sfc->setFrametimeCounter(&frametime);
      sfc->setRendertimeCounter(&rendertime);
      auto hudLayer = renderpass->createChild<KawaiiQPainterLayer>(nullptr, &drawHUD);
      renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = hudLayer, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
      renderpass->addLocalDependency(0, 1);
    }

  sfc->showMaximized();
  root.createRendererImplementation();

  return app.exec();
}
