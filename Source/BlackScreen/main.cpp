﻿#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QPainter>

namespace
{
  float frametime = -1;
  float rendertime = -1;
  float maxFrametime = std::numeric_limits<float>().min();
  float minFrametime = std::numeric_limits<float>().max();
  QString rendererName;

  void drawHUD(QPainter &p)
  {
    int yPos = p.window().height() - (15 * 3);
    p.setPen(Qt::white);

    static const QString rendererPluginStr = QStringLiteral("Renderer plugin = %1").arg(rendererName);
    p.drawText(20, yPos, rendererPluginStr);
    yPos += 15;

    if(frametime > 0 && frametime < minFrametime)
      minFrametime = frametime;

    if(frametime > maxFrametime)
      maxFrametime = frametime;

    const auto benchStr = QStringLiteral("Frametime = %1 ms [%3; %4]\t\tRendertime = %2 ms")
        .arg(frametime, 0, 'f', 3)
        .arg(rendertime, 0, 'f', 3)
        .arg(minFrametime, 0, 'f', 3)
        .arg(maxFrametime, 0, 'f', 3);
    p.drawText(20, yPos, benchStr);
  }
}

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  QCommandLineParser argParser;
  QCommandLineOption overlay("overlay", "Shows overlay with frametime and other rendering info"),
      no_overlay("no-overlay", "Hide overlay with frametime and other rendering info");

  argParser.addOptions({ overlay, no_overlay });
  auto helpOpt = argParser.addHelpOption();
  argParser.process(app);
  if(argParser.isSet(helpOpt))
    {
      argParser.showHelp();
      return 0;
    }

  bool overlayEnabled = !argParser.isSet(no_overlay) || argParser.isSet(overlay);

  qDebug("=====================");
  rendererName = KawaiiRender::getPreferredRender().getPluginName();
  qDebug("Selected renderer plugin \"%s\"", rendererName.toLatin1().data());

  KawaiiRoot root;
  KawaiiRenderpass *renderpass = root.createChild<KawaiiRenderpass>();
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  KawaiiImageSource *imgSrc = nullptr;
  KawaiiSurface *sfc = root.createChild<KawaiiSurface>(renderpass);
  sfc->resize(QSize(1280, 768));
  if(overlayEnabled)
    {
      sfc->setFrametimeCounter(&frametime);
      sfc->setRendertimeCounter(&rendertime);
      imgSrc = renderpass->createChild<KawaiiQPainterLayer>(nullptr, &drawHUD);
    }
  renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = imgSrc, .outputGBufs = {0}, .depthGBuf = std::nullopt } );

  root.createRendererImplementation();
  sfc->showMaximized();

  return app.exec();
}
