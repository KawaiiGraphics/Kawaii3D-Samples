﻿#include "Root.hpp"

#include <QDir>
#include <memory>
#include <QDebug>
#include <QGuiApplication>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  qDebug("=====================");

  std::unique_ptr<Root> world = std::make_unique<Root>();

  for(int i = 1; i < argc; ++i)
    {
      const QString fName = argv[i];
      auto fNameExt = QStringView(fName).last(fName.size() - fName.lastIndexOf('.')).trimmed();
      if(fNameExt.compare(QLatin1String(".mem")) == 0)
        world->loadMementoScene(QFile(fName));
      else
        world->loadAssimpScene(fName);
    }

  qDebug("=====================");

  world->dumpObjectTree();

  qDebug("=====================");
  qDebug("Selected renderer plugin \"%s\"", KawaiiRender::getPreferredRender().getPluginName().toLatin1().data());

  auto wnd = world->createWindow();
  wnd->show();

  int exit_code = app.exec();

  world.reset();

  return exit_code;
}
