#ifndef VIEWMANAGER_HPP
#define VIEWMANAGER_HPP

#include <QWindow>
#include <QQuaternion>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Geometry/KawaiiSkeletalAnimation.hpp>
#include <Kawaii3D/Kawaii1View.hpp>

class ViewManager : public QObject
{
  Q_OBJECT

  struct CursorState
  {
    std::unique_ptr<Kawaii1View> camMgr;
    QVector3D angle;

    QString camName;
    QString sceneName;
    QList<KawaiiCamera*>::iterator cam;

    CursorState(const QList<KawaiiCamera*>::iterator &cam = QList<KawaiiCamera*>::iterator());
    void readMatrixState();

    void updateQuaternion();
    void updateCam() const;

    inline bool operator== (const QList<KawaiiCamera*>::iterator &iter) const
    { return cam == iter; }

    CursorState& operator++();
    CursorState& operator--();
  };

  CursorState currentCamera;

  QPointF lastMousePos;
  QString rendererStr;
  enum class MouseState: uint8_t
  {
    NoButton = 0,
    LeftButton = Qt::LeftButton,
    RightButton = Qt::RightButton,
    MidButton = Qt::MiddleButton
  };
  MouseState mouse;

  KawaiiSceneLayer *sceneLayer;
  KawaiiSurface *sfc;
  KawaiiRoot *root;
  QList<KawaiiCamera*> cameras;
  QList<KawaiiSkeletalAnimation*> animations;
  QList<KawaiiSkeletalAnimation*>::iterator currentAnimation;
  std::list<QMetaObject::Connection> onVertexUpdate;

  std::chrono::steady_clock::time_point lastMinMaxFrametimeUpdated;

  float frametime;
  float minFrametime;
  float maxFrametime;

  float rendertime;

  float avgFrametime;
  float totalFrameTime;
  uint32_t totalFrameCount;

  uint32_t verticesCount;
  uint32_t trianglesCount;
  uint32_t meshCount;
  uint32_t updateBatches;
  uint32_t vertUpdates;

  void resetMinMaxFrametime();

  void switchAnimation();

  void playAnimation();
  void stopAnimation();

  void updateView();
  void switchForward();
  void switchBack();

  void toggleOffscreen();

  inline QString getAnimationName() const
  {
    return (currentAnimation!=animations.end())?
          (*currentAnimation)->objectName():
          "None";
  }

public:
  ViewManager(const QSize &size, KawaiiRoot *root);
  ~ViewManager();

  void drawHUD(QPainter &painter);

  void show();
  void showMaximized();
  void showFullScreen();

  // QObject interface
public:
  bool eventFilter(QObject *watched, QEvent *event) override;
};

#endif // VIEWMANAGER_HPP
