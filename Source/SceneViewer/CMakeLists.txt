file(GLOB_RECURSE SRC *.cpp *.hpp)

add_executable(KawaiiSceneViewer ${SRC})
target_link_libraries(KawaiiSceneViewer PUBLIC Kawaii3D::Kawaii3D)

option(FORBID_LTO "Force disable LTO for KawaiiSceneViewer" OFF)
if(NOT FORBID_LTO)
    set_property(TARGET KawaiiSceneViewer PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
    message(STATUS "KawaiiSceneViewer: Enabled LTO")
else()
    set_property(TARGET KawaiiSceneViewer PROPERTY INTERPROCEDURAL_OPTIMIZATION FALSE)
    message(STATUS "KawaiiSceneViewer: Disabled LTO")
endif()

target_include_directories (KawaiiSceneViewer PRIVATE ${INCLUDE_DIRECTORIES})

target_compile_definitions (KawaiiSceneViewer PRIVATE
    -DSHARE_DIR="${CMAKE_BINARY_DIR}/share"
    $<$<NOT:$<CONFIG:Debug>>:QT_NO_DEBUG>
    )

set_target_properties(KawaiiSceneViewer PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

add_dependencies(KawaiiSceneViewer Shaders)

install(TARGETS KawaiiSceneViewer DESTINATION bin)
