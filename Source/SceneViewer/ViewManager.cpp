#include "ViewManager.hpp"

#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/Textures/KawaiiEnvMap.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/Shaders/KawaiiScene.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <QKeyEvent>
#include <QPainter>

void ViewManager::resetMinMaxFrametime()
{
  minFrametime = std::numeric_limits<float>().max();
  maxFrametime = std::numeric_limits<float>().min();
}

void ViewManager::switchAnimation()
{
  if(currentAnimation != animations.end())
    currentAnimation++;
  else
    currentAnimation = animations.begin();

  playAnimation();
}

void ViewManager::playAnimation()
{
  if(currentAnimation != animations.end())
    {
      for(auto *i: (*currentCamera.cam)->getScene()->findChildren<KawaiiModel3D*>())
        if(auto sk = i->getSkeleton(); sk)
          sk->setAnimation(**currentAnimation);
    } else
    stopAnimation();
}

void ViewManager::stopAnimation()
{
  for(auto *i: (*currentCamera.cam)->getScene()->findChildren<KawaiiModel3D*>())
    if(auto sk = i->getSkeleton(); sk)
      sk->stopAnimation();
}

void ViewManager::updateView()
{
  for(auto &i: onVertexUpdate)
    disconnect(i);
  onVertexUpdate.clear();

  currentCamera.readMatrixState();
  resetMinMaxFrametime();

  trianglesCount = verticesCount = meshCount = 0;
  for(auto *i: (*currentCamera.cam)->getScene()->findChildren<KawaiiMesh3D*>())
    {
      verticesCount += i->vertexCount();
      trianglesCount += i->trianglesCount();
      meshCount++;
      onVertexUpdate.push_back(connect(i, &KawaiiMesh3D::verticesUpdated, [this] ([[maybe_unused]] size_t i, size_t n) { updateBatches++; vertUpdates += n; }));
    }

  playAnimation();
}

void ViewManager::switchForward()
{
  stopAnimation();
  ++currentCamera;
  if(currentCamera == cameras.end())
    currentCamera = cameras.begin();
  updateView();
}

void ViewManager::switchBack()
{
  stopAnimation();
  if(currentCamera == cameras.begin())
    currentCamera = cameras.end();
  --currentCamera;
  updateView();
}

void ViewManager::toggleOffscreen()
{
  auto fbos = root->findChildren<KawaiiFramebuffer*>();
  for(auto i: fbos)
    i->renderingEnabled = !i->renderingEnabled;

  auto envMaps = root->findChildren<KawaiiEnvMap*>();
  for(auto i: envMaps)
    i->enabled = !i->enabled;
}

ViewManager::ViewManager(const QSize &size, KawaiiRoot *root):
  QObject(root),
  mouse(MouseState::NoButton),
  root(root),
  lastMinMaxFrametimeUpdated(std::chrono::steady_clock::now()),
  frametime(-1),
  minFrametime(std::numeric_limits<float>().max()),
  maxFrametime(std::numeric_limits<float>().min()),
  rendertime(-1),
  avgFrametime(-1),
  totalFrameTime(0),
  totalFrameCount(0),
  verticesCount(0),
  trianglesCount(0),
  updateBatches(0),
  vertUpdates(0)
{
  cameras = root->findChildren<KawaiiCamera*>();

  auto scenes = root->findChildren<KawaiiScene*>();
  for(auto i: scenes)
    {
      auto scCameras = i->findChildren<KawaiiCamera*>();
      if(!scCameras.isEmpty())
        continue;

      cameras.push_back(i->createChild<KawaiiCamera>());
      cameras.back()->setViewMat(glm::lookAt(glm::vec3(0, 1, 5), glm::vec3(0,1,0), glm::vec3(0,1,0)));
      cameras.back()->setObjectName("Default_front");

      cameras.push_back(i->createChild<KawaiiCamera>());
      cameras.back()->setViewMat(glm::lookAt(glm::vec3(-5, 1, 0), glm::vec3(0,1,0), glm::vec3(0,1,0)));
      cameras.back()->setObjectName("Default_left");

      cameras.push_back(i->createChild<KawaiiCamera>());
      cameras.back()->setViewMat(glm::lookAt(glm::vec3(5, 1, 0), glm::vec3(0,1,0), glm::vec3(0,1,0)));
      cameras.back()->setObjectName("Default_right");

      cameras.push_back(i->createChild<KawaiiCamera>());
      cameras.back()->setViewMat(glm::lookAt(glm::vec3(0, 1, -5), glm::vec3(0,1,0), glm::vec3(0,1,0)));
      cameras.back()->setObjectName("Default_back");
    }

  if(cameras.isEmpty())
    throw std::runtime_error("Empty root!");

  animations = root->findChildren<KawaiiSkeletalAnimation*>();
  for(auto i = animations.begin(); i != animations.end();)
    {
      if(*i)
        {
          qDebug() << QString("Loaded animation \'%1\'").arg((*i)->objectName());
          ++i;
        }
      else
        i = animations.erase(i);
    }

  currentAnimation = animations.end();
  qDebug("Animations count = %lld", animations.size());

  currentCamera = cameras.begin();
  updateView();

  auto renderpass = root->createChild<KawaiiRenderpass>();
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
  sceneLayer = renderpass->createChild<KawaiiSceneLayer>();
  sceneLayer->setCamera(root->findChild<KawaiiCamera*>());
  sceneLayer->getProjectionPtr()->setFarClip(500);
  renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = sceneLayer, .outputGBufs = {0}, .depthGBuf = 1 } );
  auto hudLayer = renderpass->createChild<KawaiiQPainterLayer>(nullptr, std::bind(&ViewManager::drawHUD, this, std::placeholders::_1));
  renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = hudLayer, .outputGBufs = {0}, .depthGBuf = std::nullopt } );
  renderpass->addLocalDependency(0, 1);

  sfc = root->createChild<KawaiiSurface>(renderpass);
  sfc->setFrametimeCounter(&frametime);
  sfc->setRendertimeCounter(&rendertime);
  sfc->resize(size);
  sfc->getWindow().installEventFilter(this);

  root->createRendererImplementation();
  rendererStr = QStringLiteral("Renderer plugin = %1").arg(KawaiiRender::getPreferredRender().getPluginName());
}

ViewManager::~ViewManager()
{
  sfc->setFrametimeCounter(nullptr);
}

void ViewManager::drawHUD(QPainter &painter)
{
  painter.setPen(Qt::yellow);
  int y = painter.window().height() - (10 * 15);

  painter.drawText(10,y, QStringLiteral("Scene %1").arg(currentCamera.sceneName));
  y += 15;

  painter.drawText(10,y, QStringLiteral("%1 meshes\t%2 triangles;\t%3 vertices").arg(meshCount).arg(trianglesCount).arg(verticesCount));
  y += 15;

  painter.drawText(10,y, QStringLiteral("%1 update batches;\t%2 vertex updates").arg(updateBatches).arg(vertUpdates));
  y += 15;

  painter.drawText(10,y, QStringLiteral("Animation = %1").arg(getAnimationName()));
  y += 15;

  painter.drawText(10,y, QStringLiteral("Camera name = \"%1\"").arg(currentCamera.camName));
  y += 15;

  painter.drawText(10,y, QStringLiteral("Camera angle = (%1; %2; %3)").arg(currentCamera.angle.x()).arg(currentCamera.angle.y()).arg(currentCamera.angle.z()));
  y += 15;

  auto &pos = currentCamera.camMgr->getPosition();
  painter.drawText(10,y, QStringLiteral("Camera position = (%1; %2; %3)").arg(pos.x()).arg(pos.y()).arg(pos.z()));
  y += 15;

  painter.drawText(10,y, rendererStr);
  y += 15;

  if(frametime > 0 && frametime < minFrametime)
    minFrametime = frametime;

  if(frametime > maxFrametime)
    maxFrametime = frametime;

  if(avgFrametime == -1)
    avgFrametime = frametime;

  totalFrameTime += frametime;
  totalFrameCount++;

  const auto t = std::chrono::steady_clock::now();
  if(std::chrono::duration_cast<std::chrono::milliseconds>(t-lastMinMaxFrametimeUpdated).count() >= 1000)
    {
      minFrametime = frametime;
      maxFrametime = frametime;
      avgFrametime = totalFrameTime / static_cast<float>(totalFrameCount);
      totalFrameCount = 0;
      totalFrameTime = 0;
      lastMinMaxFrametimeUpdated = t;
    }

  const auto benchStr = QStringLiteral("Frametime = %1 ms [%3; %4]\t\tRendertime = %2 ms\t\tAverage frametime = %5 ms").arg(frametime, 0, 'f', 3).arg(rendertime, 0, 'f', 3).arg(minFrametime, 0, 'f', 3).arg(maxFrametime, 0, 'f', 3).arg(avgFrametime, 0, 'f', 3);
  painter.drawText(10,y, benchStr);

  updateBatches = vertUpdates = 0;
}

void ViewManager::show()
{
  sfc->show();
}

void ViewManager::showMaximized()
{
  sfc->showMaximized();
}

void ViewManager::showFullScreen()
{
  sfc->showFullScreen();
}

bool ViewManager::eventFilter(QObject *watched, QEvent *event)
{
  switch(event->type())
    {
    case QEvent::KeyRelease:
      {
        switch(static_cast<QKeyEvent*>(event)->key())
          {
          case Qt::Key_Tab:
            if(static_cast<QKeyEvent*>(event)->modifiers().testFlag(Qt::ShiftModifier))
              switchBack();
            else
              switchForward();

            sceneLayer->setCamera(*currentCamera.cam);
            break;

          case Qt::Key_Backtab:
            switchBack();
            sceneLayer->setCamera(*currentCamera.cam);
            break;

          case Qt::Key_Enter:
          case Qt::Key_Z:
            switchAnimation();
            break;

          case Qt::Key_Space:
            toggleOffscreen();
            break;
          }
      }
      break;

    case QEvent::MouseMove:
      {
        if(mouse == MouseState::NoButton)
          break;

        auto m_ev = static_cast<QMouseEvent*>(event);

        QPointF mouseDelta = m_ev->pos() - lastMousePos;
        lastMousePos = m_ev->pos();

        float w = watched->property("width").toFloat(),
            h = watched->property("height").toFloat();

        if(mouse == MouseState::RightButton)
          {
            QVector2D angleDelta(mouseDelta.y() * 180.0f / h, -mouseDelta.x() * 180.0f / w);
            currentCamera.angle += QVector3D(angleDelta, 0);

            currentCamera.updateQuaternion();
            currentCamera.updateCam();
          } else
          {
            bool shift = static_cast<QMouseEvent*>(event)->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier);

            QVector2D delta(mouseDelta.x() * 1.5f / w, mouseDelta.y() * 1.5f / h);
            if(mouse == MouseState::LeftButton)
              {
                auto tr = Kawaii1View::Transaction(currentCamera.camMgr);
                QVector3D dPos = tr.rotation.rotatedVector(QVector3D(delta.x(), 0, delta.y()));
                if(shift)
                  dPos *= 10.0;
                tr.position += dPos;
              } else
              {
                auto tr = Kawaii1View::Transaction(currentCamera.camMgr);
                QVector3D dPos = tr.rotation.rotatedVector(QVector3D(delta, 0));
                if(shift)
                  dPos *= 10.0;
                tr.position += dPos;
              }
            currentCamera.updateCam();
          }
        break;
      }

    case QEvent::MouseButtonRelease:
      lastMousePos = static_cast<QMouseEvent*>(event)->pos();
      mouse = MouseState::NoButton;
      break;

    case QEvent::MouseButtonPress:
      lastMousePos = static_cast<QMouseEvent*>(event)->pos();
      mouse = static_cast<MouseState>(static_cast<QMouseEvent*>(event)->button());
      break;

    default: break;
    }

  return false;
}


ViewManager::CursorState::CursorState(const QList<KawaiiCamera*>::iterator &cam):
  angle(0,0,0),
  camName("No camera"),
  sceneName("No scene"),
  cam(cam)
{
}

void ViewManager::CursorState::readMatrixState()
{
  camName = (*cam)->objectName();
  sceneName = (*cam)->getScene()?
        (*cam)->getScene()->objectName():
        "No scene";

  if(!camMgr)
    camMgr.reset(new Kawaii1View(**cam));
  angle = camMgr->getRotation().toEulerAngles();
}

void ViewManager::CursorState::updateQuaternion()
{
  angle.setX(angle.x() - 360.0 * std::trunc(angle.x() / 360.0));
  angle.setY(angle.y() - 360.0 * std::trunc(angle.y() / 360.0));
  angle.setZ(angle.z() - 360.0 * std::trunc(angle.z() / 360.0));

  camMgr->setRotation(QQuaternion::fromEulerAngles(angle));
}

void ViewManager::CursorState::updateCam() const
{
  (*cam)->setViewMat(camMgr->getCameraMatrix());
}

ViewManager::CursorState& ViewManager::CursorState::operator++()
{
  *this = CursorState(cam + 1);
  return *this;
}

ViewManager::CursorState& ViewManager::CursorState::operator--()
{
  *this = CursorState(cam - 1);
  return *this;
}
