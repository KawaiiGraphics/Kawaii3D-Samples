#include "Root.hpp"
#include "ViewManager.hpp"

#include <QDebug>
#include <unordered_map>
#include <sib_utils/Strings.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <sib_utils/dirty_cast.hpp>
#include <sib_utils/Memento/BlobLoader.hpp>

#include <Kawaii3D/KawaiiBufBinding.hpp>
#include <Kawaii3D/AssetZygote/KawaiiSubsceneZygote.hpp>

#include <Kawaii3D/Textures/KawaiiImage.hpp>

#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <Kawaii3D/Illumination/KawaiiIllumination.hpp>


inline uint qHash(const QColor &col)
{
  return qHash(sib_utils::dirty_cast<uint64_t>(col.rgba64()));
}

class MaterialReplicator: public KawaiiMaterialReplicator
{
  QHash<QColor, KawaiiImage*> singleColorTex;
  std::unordered_map<KawaiiTexture*, KawaiiMaterial*> materials;
  std::unordered_map<std::string, KawaiiImage*> loadedTextures;

  KawaiiScene *sc;
  KawaiiMaterial *coloredMaterial;
  KawaiiProgram &texturedShader;
  KawaiiProgram &coloredShader;
public:
  MaterialReplicator(KawaiiProgram &texturedShader, KawaiiProgram &coloredShader):
    sc(nullptr),
    coloredMaterial(nullptr),
    texturedShader(texturedShader),
    coloredShader(coloredShader)
  {
  }

  void setScene(KawaiiScene *scene)
  {
    if(sc != scene)
      {
        sc = scene;
        materials.clear();

        //Legal because in current version there is no way to select one scene multiple times
        coloredMaterial = sc->createChild<KawaiiMaterial>();
        coloredMaterial->setObjectName("MaterialColored");
        sc->setProgramToMaterial(coloredMaterial, &coloredShader);
      }
  }

  // KawaiiMaterialReplicator interface
private:
  KawaiiTexture *replicateTexture([[maybe_unused]] KawaiiTextureRole texRole, [[maybe_unused]] size_t texId) override
  {
    if(auto fName = getTextureParam<std::string>(KawaiiTextureParamRole::FileName); fName.has_value())
      {
        auto texData = loadedTextures.find(*fName);
        if(texData == loadedTextures.end())
          {
            QString q_fName = QString::fromStdString(*fName);
            if(QImage img; img.load(q_fName))
              {
                auto tex = new KawaiiImage();
                tex->setObjectName(q_fName);
                tex->changeImage([&img] (QImage &d) { d.swap(img); return true; });
                loadedTextures.insert({*fName, tex});
                return tex;
              } else
              {
                qWarning().noquote() << "Could not load texture file \"" << q_fName << "\"!";
                return nullptr;
              }
          }
        return texData->second;
      } else
      return nullptr;
  }

  KawaiiMaterial *texturedMaterial(KawaiiTexture *tex)
  {
    auto el = materials.find(tex);
    if(el == materials.end())
      {
        //In KawaiiSceneViewer materials should be unique to each scene in order to render different object sets
        auto *material = sc->createChild<KawaiiMaterial>();

        materials.insert({tex, material});
        sc->setProgramToMaterial(material, &texturedShader);

        auto uniforms = material->createChild<KawaiiGpuBuf>(nullptr, 0);
        uniforms->bindTexture(QStringLiteral("tex"), tex);
        material->setUniforms(uniforms);

        return material;
      }
    return el->second;
  }

  KawaiiMaterial *replicateMaterial() override
  {
    if(auto tex = getTexture(KawaiiTextureRole::Diffuse, 0); tex)
      {
        return texturedMaterial(tex);
      } else
      {
        bool colored = false;
        QColor col;
        if(auto v3Param = getParam<QVector3D>(KawaiiRenderParamRole::ColorDiffuse); v3Param)
          {
            colored = true;
            col.setRgbF(v3Param->x(), v3Param->y(), v3Param->z());
          } else
          if(auto v4Param = getParam<QVector4D>(KawaiiRenderParamRole::ColorDiffuse); v4Param)
            {
              colored = true;
              col.setRgbF(v4Param->x(), v4Param->y(), v4Param->z(), v4Param->w());
            }
        if(colored)
          {
            auto tex = singleColorTex.value(col);
            if(!tex)
              {
                QImage singleColorImg(QSize(1,1), QImage::Format_RGBA8888);
                singleColorImg.setPixelColor(0,0, col);

                tex = sc->createChild<KawaiiImage>();
                tex->changeImage([&singleColorImg] (QImage &img) { img.swap(singleColorImg); return true; });

                singleColorTex.insert(col, tex);
              }
            return texturedMaterial(tex);
          } else
          return coloredMaterial;
      }
  }
};

namespace {
#ifdef SHARE_DIR
  const QDir shareDir = QDir(SHARE_DIR);
#else
  const QDir shareDir = QDir::current();
#endif
}

const QString Root::vertTexShaderSrc = sib_utils::ioReadAll(QFile(shareDir.absoluteFilePath("Shaders/TexturedShader/main.vs.glsl")));
const QString Root::fragTexShaderSrc = sib_utils::ioReadAll(QFile(shareDir.absoluteFilePath("Shaders/TexturedShader/main.fs.glsl")));

const QString Root::vertGrayShaderSrc = sib_utils::ioReadAll(QFile(shareDir.absoluteFilePath("Shaders/GrayShader/main.vs.glsl")));
const QString Root::fragGrayShaderSrc = sib_utils::ioReadAll(QFile(shareDir.absoluteFilePath("Shaders/GrayShader/main.fs.glsl")));

void Root::createIllumination()
{
  KawaiiGpuBuf *gammaGpuBuf = createChild<KawaiiGpuBuf>(&gammaBuf, sizeof(gammaBuf));
  gammaGpuBuf->setObjectName("GammaBuffer");
  gammaGpuBuf->createChild<KawaiiBufBinding>()->setBindingPoint(3);

  KawaiiIllumination *illumination = createChild<KawaiiIllumination>("PBR");
  illumination->createLampArrays<KawaiiPbrLight>();
  illumination->createUBO(2);

  illumination->getLampsLayout().addDirLight();
  auto &l = static_cast<KawaiiIlluminationStructs<KawaiiPbrLight>::DirLight&>(
        illumination->getLampsLayout().getDirLight(0));

  l.direction = QVector3D(0, -1, 0);

  l.ambient = QVector3D(0.25, 0.25, 0.25);
  l.color = QVector3D(1, 1, 1);
  illumination->getLampsLayout().updateDirLight(0);

  if(!illumination->tryEnable())
    qDebug("Illumination creating failed!");
}

void Root::createAssimpViewer()
{
  texturedAssimpViewer.reset(createChild<KawaiiProgram>());
  texturedAssimpViewer->createShaderModule(KawaiiShaderType::Vertex)->setSource(vertTexShaderSrc);
  texturedAssimpViewer->createShaderModule(KawaiiShaderType::Fragment)->setSource(fragTexShaderSrc);
  texturedAssimpViewer->setCullMode(KawaiiProgram::CullMode::CullBack);

  metallicAssimpViewer.reset(createChild<KawaiiProgram>());
  metallicAssimpViewer->createShaderModule(KawaiiShaderType::Vertex)->setSource(vertGrayShaderSrc);
  metallicAssimpViewer->createShaderModule(KawaiiShaderType::Fragment)->setSource(fragGrayShaderSrc);
  metallicAssimpViewer->setCullMode(KawaiiProgram::CullMode::CullBack);

  materialReplicator.reset(new MaterialReplicator(*texturedAssimpViewer, *metallicAssimpViewer));
}

Root::Root():
  gammaBuf { 0.75, 2.2 }
{
  createIllumination();
}

void Root::loadMementoScene(QFile &&f)
{
  std::unique_ptr<sib_utils::memento::Memento> rootMemento;
  rootMemento.reset(sib_utils::memento::BlobLoader().load(f));
  KawaiiDataUnit::getMementoFactory().loadChildren(*rootMemento, this);
}

void Root::loadAssimpScene(const QString &fileName)
{
  if(!texturedAssimpViewer)
    createAssimpViewer();

  KawaiiScene *scene = createChild<KawaiiScene>();
  scene->loadAsset(fileName);
  scene->setObjectName(fileName.mid(fileName.lastIndexOf('/')+1));
  if(auto subsc = scene->findChild<KawaiiSubsceneZygote*>(); subsc)
    {
      static_cast<MaterialReplicator&>(*materialReplicator).setScene(scene);
      subsc->replicate(*this, *materialReplicator);
      delete subsc;
    }
}

ViewManager* Root::createWindow()
{
  return new ViewManager(QSize(1280, 768), this);
}
