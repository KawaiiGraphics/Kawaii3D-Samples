#ifndef ROOT_HPP
#define ROOT_HPP

#include <Kawaii3D/AssetZygote/KawaiiMaterialReplicator.hpp>
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include "ViewManager.hpp"

class KawaiiImage;

class Root: public KawaiiRoot
{
  static const QString vertTexShaderSrc;
  static const QString fragTexShaderSrc;

  static const QString vertGrayShaderSrc;
  static const QString fragGrayShaderSrc;

  std::unique_ptr<KawaiiProgram> texturedAssimpViewer;
  std::unique_ptr<KawaiiProgram> metallicAssimpViewer;
  std::unique_ptr<KawaiiMaterialReplicator> materialReplicator;

  void createIllumination();
  void createAssimpViewer();
public:
  struct Gamma {
    float exposure;
    float gamma;
  } gammaBuf;

  Root();

  void loadMementoScene(QFile &&f);

  void loadAssimpScene(const QString &fileName);

  ViewManager *createWindow();
};

#endif // ROOT_HPP
