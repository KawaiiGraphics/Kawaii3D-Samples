#include "SimpleScene.hpp"

#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include <Kawaii3D/KawaiiBufBinding.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <QMatrix4x4>
#include <QVector2D>
#include <QPainter>
#include <QDir>

#ifdef SHARE_DIR
const QDir SimpleScene::shareDir = QDir(SHARE_DIR);
#else
const QDir SimpleScene::shareDir = QDir::current();
#endif

SimpleScene::SimpleScene(QFuture<void> &saveTask, int flags):
  saveTask(saveTask),
  overlayTxtLines(flags & Overlay?
                    QString("Hello, overlay!"):
                    QString()),
  w(0),
  h(0),

  mainCamera(nullptr),
  fboCamera(nullptr),
  material(nullptr),
  textureImg(nullptr),
  imgSender(nullptr),
  offscrRp0(nullptr),
  offscrSc0(nullptr),
  fbo0(nullptr),
  textureFbo0(nullptr),
  offscrRp1(nullptr),
  offscrSc1(nullptr),
  fbo1(nullptr),
  textureFbo1(nullptr),
  torusModel(nullptr),
  cubeModel(nullptr),
  rectanglesModel(nullptr),
  shaderpack(nullptr),
  frametime(-1),
  minFrametime(std::numeric_limits<float>().max()),
  maxFrametime(std::numeric_limits<float>().min())
{
  setObjectName("KawaiiDemoScene");

  createCameras();
  createMaterial();
  if(flags & Textures)
    {
      overlayTxtLines.append("Textures: enabled");
      createTexture();
      if(flags & FBOs)
        {
          createFbo0();
          createFbo1();
          overlayTxtLines.append("Off-screen rendering: enabled");
        } else
        overlayTxtLines.append("Off-screen rendering: disabled");
    } else
    overlayTxtLines.append("Textures: disabled");

  overlayTxtLines.append("Renderer plugin: " + KawaiiRender::getPreferredRender().getPluginName());

  createTorus();
  createRectangles();
  createCube();

  createShaderpack();
  createGlobalUboBinding();
}

KawaiiCamera* SimpleScene::getMainCamera() const
{
  return mainCamera;
}

KawaiiCamera* SimpleScene::getFboCamera() const
{
  return fboCamera;
}

void SimpleScene::drawOverlayText(QPainter &painter)
{
  painter.setPen(Qt::white);
  int yPos = painter.window().height() - (15 * (4 + overlayTxtLines.size()));
  for(const auto &overlayText: overlayTxtLines)
    {
      painter.drawText(20, yPos, overlayText);
      yPos += 15;
    }

  if(frametime > 0 && frametime < minFrametime)
    minFrametime = frametime;

  if(frametime > maxFrametime)
    maxFrametime = frametime;

  const auto benchStr = QString("Frametime = %1 ms [%3; %4]\t\tRendertime = %2 ms").arg(frametime, 0, 'f', 3).arg(rendertime, 0, 'f', 3).arg(minFrametime, 0, 'f', 3).arg(maxFrametime, 0, 'f', 3);
  painter.drawText(20, yPos, benchStr);
  yPos += 15;

  if(saveTask.isStarted())
    {
      painter.drawText(20, yPos, "Saving started...");
      yPos += 15;
    }

  if(saveTask.isFinished())
    {
      painter.drawText(20, yPos, "Saving finished!");
      yPos += 15;
    }
}

bool SimpleScene::isOverlayEnabled() const
{
  return !overlayTxtLines.isEmpty() && !overlayTxtLines.at(0).isNull();
}

void SimpleScene::createCameras()
{
  mainCamera = createChild<KawaiiCamera>();
  mainCamera->setObjectName("MainCamera");
  mainCamera->setViewMat(glm::lookAt(glm::vec3(0.0f, 1.0f, -1.5f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0, 1.0f, 0)));

  fboCamera = createChild<KawaiiCamera>();
  fboCamera->setObjectName("FboCamera");
  fboCamera->setViewMat(glm::lookAt(glm::vec3(1.5f, 0.0f, -1.5f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0, 1.0f, 0)));
}

void SimpleScene::createMaterial()
{
  material = createChild<KawaiiMaterial>();
  Q_SET_OBJECT_NAME(material);
  auto materialUniforms = createGpuBuf(UniformData { glm::vec4(0.3,  0.75, 0.5,  1.0) },
                                       material,
                                       QLatin1String("MaterialUniforms"));

  material->setUniforms(materialUniforms);
}

void SimpleScene::createTexture()
{
  textureImg = createChild<KawaiiImage>();
  textureImg->setObjectName("ImageTexture");
  textureImg->loadAsset(shareDir.absoluteFilePath("Pictures/01.png"));

  int h0 = (*textureImg)->height();
  int w0 = (*textureImg)->width();
  //    new Timebomb(img, 1500, 200);

  h = h0;
  w = w0;
  /*img = new KawaiiImage;
  (*img)->load(shareDir.absoluteFilePath("Pictures/Minna.jpg"));
  img->setPosition(QPoint(0, h0));
  img->updateParent(textureImg);
  img->setObjectName("BottomImage");
  h = h0 + (*img)->height();
  w = std::max((*img)->width(), w0);*/

  float size = 0.75,
      maxDim = std::max(w, h);

  w *= size / maxDim;
  h *= size / maxDim;

  imgSender = createTexSender(textureImg);
}

void SimpleScene::createFbo0()
{
  offscrRp0 = createChild<KawaiiRenderpass>();
  offscrRp0->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  offscrRp0->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
  offscrSc0 = offscrRp0->createChild<KawaiiSceneLayer>();
  offscrSc0->setCamera(fboCamera);
  offscrRp0->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = offscrSc0, .outputGBufs = {0}, .depthGBuf = 1 } );

  fbo0 = createChild<KawaiiFramebuffer>(glm::uvec2(1024, 1024));
  fbo0->setObjectName("Framebuffer0");
  fbo0->setRenderpass(offscrRp0);

  QImage img(fbo0->getSize().x, fbo0->getSize().y, QImage::Format_RGBA8888);
  textureFbo0 = fbo0->createChild<KawaiiImage>();
  textureFbo0->setImage(std::move(img));
  fbo0->attachTexture(0, textureFbo0);
}

void SimpleScene::createFbo1()
{
  offscrRp1 = createChild<KawaiiRenderpass>();
  offscrRp1->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
  offscrRp1->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
  offscrSc1 = offscrRp1->createChild<KawaiiSceneLayer>();
  offscrSc1->setCamera(mainCamera);
  offscrRp1->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = offscrSc1, .outputGBufs = {0}, .depthGBuf = 1 } );

  fbo1 = createChild<KawaiiFramebuffer>(glm::uvec2(512, 512));
  fbo1->setObjectName("Framebuffer1");
  fbo1->setRenderpass(offscrRp1);

  QImage img(fbo1->getSize().x, fbo1->getSize().y, QImage::Format_RGBA8888);
  textureFbo1 = fbo1->createChild<KawaiiImage>();
  textureFbo1->setImage(std::move(img));
  fbo1->attachTexture(0, textureFbo1);
}

void SimpleScene::createTorus()
{
  QMatrix4x4 mat;
  mat.rotate(-45, 1,0,0);

  torusModel = createFigure("Torus", mat, imgSender);
}

void SimpleScene::createCube()
{
  QMatrix4x4 mat;
  mat.translate(0, -1, 1);
  mat.rotate(45, 0,0,1);

  cubeModel = createFigure("Cube", mat, createTexSender(textureFbo1));
}

void SimpleScene::createRectangles()
{
  rectanglesModel = createChild<KawaiiModel3D>();
  auto meshInstance = createRectangle(QVector3D(-1.5, 0, 0.5))->createChild<KawaiiMeshInstance>(material, imgSender);
  meshInstance->setObjectName("Instance_0");

  w = 0.75;
  h = 0.75;
  meshInstance = createRectangle(QVector3D(1.5, 0, -0.5))->createChild<KawaiiMeshInstance>(material, createTexSender(textureFbo0));
  meshInstance->setObjectName("Instance_1");
}

void SimpleScene::createShaderpack()
{
  shaderpack = createChild<KawaiiProgram>();
  shaderpack->setObjectName("GLSLProgram");
  shaderpack->createShaderModule(KawaiiShaderType::Vertex, QFile(shareDir.absoluteFilePath("Shaders/SimpleSceneShader/main.vs.glsl")));
  shaderpack->createShaderModule(KawaiiShaderType::Fragment, QFile(shareDir.absoluteFilePath("Shaders/SimpleSceneShader/main.fs.glsl")));
  shaderpack->createShaderModule(KawaiiShaderType::Fragment, QFile(shareDir.absoluteFilePath("Shaders/SimpleSceneShader/textureHelper.fs.glsl")));
  shaderpack->setCullMode(KawaiiProgram::CullMode::CullBack);
  setProgramToMaterial(material, shaderpack);
}

KawaiiModel3D *SimpleScene::createFigure(const QString &figure, const QMatrix4x4 &trMat, KawaiiGpuBuf *ubo)
{
  QString assetName(":/" + figure.toLower() + "_x0.5");

  KawaiiModel3D *result;

  try {
    result = createChild<KawaiiModel3D>();
    result->loadAsset(assetName, trMat);
  }
  catch(KawaiiPluginNotFound&)
  {
    qDebug("warn: plugin KawaiiFigures3D is not installed");
    result = nullptr;
  }
  catch(KawaiiPluginFailed&)
  {
    qDebug("warn: your plugin KawaiiFigures3D is broken");
    result = nullptr;
  }

  if(!result)
    return result;

  auto instance = result->findChild<KawaiiMesh3D*>()->createChild<KawaiiMeshInstance>(material, ubo);
  instance->setObjectName("TorusInstance");
  return result;
}

KawaiiMesh3D *SimpleScene::createRectangle(const QVector3D &position)
{
  QVector3D normal(0,0,1);

  auto mesh = rectanglesModel->addMesh(QString("Rectangle_%1").arg(rectanglesModel->findChildren<KawaiiMesh3D*>().count() + 1));

  mesh->addVertex(QVector4D(-w,  h, 1, 1) + QVector4D(position, 0.0), normal, QVector3D(1,0,0)); //Left Top;     0
  mesh->addVertex(QVector4D( w,  h, 1, 1) + QVector4D(position, 0.0), normal, QVector3D(0,0,0)); //Right Top;    1
  mesh->addVertex(QVector4D( w, -h, 1, 1) + QVector4D(position, 0.0), normal, QVector3D(0,1,0)); //Roght Bottom; 2
  mesh->addVertex(QVector4D(-w, -h, 1, 1) + QVector4D(position, 0.0), normal, QVector3D(1,1,0)); //Left  Bottom; 3

  mesh->addTriangle(0, 1, 2);
  mesh->addTriangle(0, 2, 3);

  return mesh;
}

KawaiiGpuBuf* SimpleScene::createGpuBuf(const SimpleScene::UniformData &d, sib_utils::TreeNode *parent, const QLatin1String &objectName)
{
  KawaiiGpuBuf *buf = parent->createChild<KawaiiGpuBuf>(&d, sizeof(KawaiiGpuBuf));
  buf->setObjectName(objectName);
  return buf;
}

KawaiiGpuBuf* SimpleScene::createGpuBuf(const UniformData &d, const QLatin1String &objectName)
{
  return createGpuBuf(d, this, objectName);
}

KawaiiGpuBuf *SimpleScene::createTexSender(KawaiiTexture *tex)
{
  if(!tex)
    return nullptr;

  auto texSender = createChild<KawaiiGpuBuf>();
  Q_SET_OBJECT_NAME(texSender);
  texSender->bindTexture("tex", tex);
  return texSender;
}

void SimpleScene::createGlobalUboBinding()
{
  static const QVector2D d(0.5, 0.5);

  KawaiiGpuBuf *buf =  createChild<KawaiiGpuBuf>(&d, sizeof(d));
  buf->setObjectName("globalUbo");

  auto bgImg = buf->createChild<KawaiiImage>();
  bgImg->setObjectName("BgImageTexture");
  bgImg->loadAsset(shareDir.absoluteFilePath("Pictures/bg.png"));

  buf->bindTexture("bgTexture", bgImg);

  KawaiiBufBinding *binding = buf->createChild<KawaiiBufBinding>();
  binding->setObjectName("globalUbo-binding");
  binding->setBindingPoint(1);
}

void SimpleScene::timerEvent(QTimerEvent *)
{
  if(frametime != -1)
    qDebug("\tFrametime = %.3f ms\t\tRendertime = %.3f ms", frametime, rendertime);
}
