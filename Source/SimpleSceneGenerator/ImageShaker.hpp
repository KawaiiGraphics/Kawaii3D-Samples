#ifndef IMAGESHAKER_HPP
#define IMAGESHAKER_HPP

#include <Kawaii3D/Textures/KawaiiImage.hpp>

class ImageShaker : public QObject
{
  Q_OBJECT

  KawaiiImage *target;
  int amplitude;
  int timerId;
  bool shaked;

public:
  ImageShaker(KawaiiImage *img, int interval, int amplitude);
  ~ImageShaker();

  // QObject interface
protected:
  void timerEvent(QTimerEvent *event) override;
};

#endif // IMAGESHAKER_HPP
