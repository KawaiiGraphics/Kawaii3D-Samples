#include "ImageShaker.hpp"
#include <QTimerEvent>

ImageShaker::ImageShaker(KawaiiImage *img, int interval, int amplitude):
  QObject(img),
  target(img),
  amplitude(amplitude),
  shaked(false)
{
  timerId = startTimer(interval);
}

ImageShaker::~ImageShaker()
{
  killTimer(timerId);
}

void ImageShaker::timerEvent(QTimerEvent *event)
{
  if(event->timerId() == timerId)
    {
      /*target->setPosition(QPoint(shaked? 0: amplitude,
                                 target->getRegion().first.y));*/
    } else
    QObject::timerEvent(event);
}
