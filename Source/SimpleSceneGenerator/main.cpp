﻿#include "SimpleScene.hpp"

#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/Surfaces/KawaiiRender.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

#include <sib_utils/Memento/BlobSaver.hpp>
#include <QGuiApplication>
#include <QtConcurrent>


/*namespace {
  class MeshRapist: public QObject
  {
    KawaiiMesh3D *mesh;
    float h;
  public:
    MeshRapist(KawaiiMesh3D *mesh, float h, int interval):
      QObject(mesh),
      mesh(mesh), h(h)
    {
      startTimer(interval);
    }

    // QObject interface
  protected:
    void timerEvent(QTimerEvent *) override
    {
      mesh->addVertex(QVector4D(0.5, 0.5+h*2.0, 0.9, 1), QVector3D(0,0,0), QVector3D(0.5, -0.5, 0));
      mesh->addTriangle(0, 1, 4);
      deleteLater();
    }
  };

  class CamRotor: public QObject
  {
  public:
    CamRotor(KawaiiCamera *cam):
      QObject(cam),
      angle(90.0),
      v0(3.33)
    {
      startTimer(10);
    }

    // QObject interface
  protected:
    float angle;
    float v0;

    void timerEvent(QTimerEvent *) override
    {

      if(auto cam = static_cast<KawaiiCamera*>(parent()); cam)
        {
          angle += 0.01 * v0;
          v0 -= 0.0001 * (angle - 90.0);

          auto mat = glm::rotate(glm::mat4(), angle, glm::vec3(0.0, 1.0, 0.0));
          cam->setViewMat(glm::lookAt(glm::vec3(mat * glm::vec4(0.0, 0.0, 1.0 ,1.0)), glm::vec3(0.0), glm::vec3(0.0, 1.0, 0.0)));
        }
    }
  };
}*/

int main(int argc, char *argv[])
{
  QCommandLineParser argParser;
  QCommandLineOption
      textures("textures", "Use textures for building the simple scene"),
      no_textures("no-textures", "Do not use textures for building the simple scene"),

      fbo("fbo", "Enable rendering to texture"),
      no_fbo("no-fbo", "Disable rendering to texture"),

      overlay("overlay", "Shows overlay with frametime and other rendering info"),
      no_overlay("no-overlay", "Hide overlay with frametime and other rendering info"),

      headlessOpt("headless", "Do not render the simple scene, just save it\'s memento");

  argParser.addOptions({textures, no_textures, fbo, no_fbo, overlay, no_overlay, headlessOpt});
  auto helpOpt = argParser.addHelpOption();
  QStringList cmdArgs;
  cmdArgs.reserve(argc);
  for(int i = 0; i < argc; ++i)
    cmdArgs.push_back(argv[i]);
  argParser.parse(cmdArgs);

  bool headless = argParser.isSet(headlessOpt);
  std::unique_ptr<QCoreApplication> app = headless?
        std::make_unique<QCoreApplication>(argc, argv):
        std::make_unique<QGuiApplication>(argc, argv);

  if(argParser.isSet(helpOpt))
    {
      argParser.showHelp();
      return 0;
    }

  int flags = 0;
  if(argParser.isSet(textures) || !argParser.isSet(no_textures))
    flags |= SimpleScene::Textures;

  if(argParser.isSet(fbo) || !argParser.isSet(no_fbo))
    flags |= SimpleScene::FBOs;

  if(argParser.isSet(overlay) || !argParser.isSet(no_overlay))
    flags |= SimpleScene::Overlay;

  qDebug("=====================");

  QFuture<void> mementoWork;
  KawaiiRoot *root = new KawaiiRoot;
  auto sc = root->createChild<SimpleScene>(mementoWork, flags);

  root->dumpObjectTree();
  qDebug("=====================");
  qDebug("Selected renderer plugin \"%s\"", KawaiiRender::getPreferredRender().getPluginName().toLatin1().data());

  KawaiiSurface *sfc = nullptr;
  KawaiiRenderpass *renderpass = nullptr;
  if(!headless)
    {
      renderpass = root->createChild<KawaiiRenderpass>();
      renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm, .needClear = true, .needStore = true });
      renderpass->addGBuf(KawaiiRenderpass::GBuffer { .format = KawaiiRenderpass::InternalImgFormat::Depth16, .needClear = true, .needStore = false });
      KawaiiSceneLayer *sceneLayer = renderpass->createChild<KawaiiSceneLayer>();
      sceneLayer->setCamera(root->findChild<KawaiiCamera*>());
      renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = sceneLayer, .outputGBufs = {0}, .depthGBuf = 1 } );

      sfc = root->createChild<KawaiiSurface>(renderpass);
      sfc->resize(QSize(1280, 768));
      sfc->setFrametimeCounter(&sc->frametime);
      sfc->setRendertimeCounter(&sc->rendertime);

      if(sc->isOverlayEnabled())
        {
          auto hudLayer = renderpass->createChild<KawaiiQPainterLayer>(nullptr, std::bind(&SimpleScene::drawOverlayText, sc, std::placeholders::_1));
          renderpass->addSubpass(KawaiiRenderpass::Subpass { .imgSrc = hudLayer, .outputGBufs = {0} } );
          renderpass->addLocalDependency(0, 1);
        } else
        sc->startTimer(1000);

      root->createRendererImplementation();
      sfc->showMaximized();
    }

  QThreadPool mementoSaverPool;
  mementoSaverPool.setMaxThreadCount(1);

  mementoWork = QtConcurrent::run(&mementoSaverPool, [root, headless, &app] {
      auto rootMemento = KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(*root);
      SimpleScene::getShareDir().mkdir("Scenes");
      sib_utils::memento::BlobSaver().save(QFile(SimpleScene::getShareDir().absoluteFilePath("Scenes/DefaultKawaiiScene.mem")), *rootMemento);
      delete rootMemento;
      if(headless)
        app->quit();
    });

  int exit_code = app->exec();

  if(!headless && sc->isOverlayEnabled())
    {
      renderpass->removeSubpass(1);
      if(auto layer = renderpass->findChild<KawaiiQPainterLayer*>(); layer)
        delete layer;
    }

  mementoWork.waitForFinished();

  delete root;

  return exit_code;
}
