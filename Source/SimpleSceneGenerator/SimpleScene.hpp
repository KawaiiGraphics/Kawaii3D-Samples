#ifndef SIMPLESCENE_HPP
#define SIMPLESCENE_HPP

#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/Textures/KawaiiImage.hpp>
#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include <Kawaii3D/KawaiiCamera.hpp>
#include <glm/vec4.hpp>

class SimpleScene : public KawaiiScene
{
  Q_OBJECT
public:
  enum Feature
  {
    Textures = 1,
    FBOs = 2,
    Overlay = 4
  };

  SimpleScene(QFuture<void> &saveTask, int flags = Textures | FBOs | Overlay);

  inline static const QDir& getShareDir()
  { return shareDir; }

  KawaiiCamera *getMainCamera() const;
  KawaiiCamera *getFboCamera() const;

  void drawOverlayText(QPainter &painter);
  bool isOverlayEnabled() const;

private:
  static const QDir shareDir;
  struct UniformData {
    glm::vec4 color;
  };

  QFuture<void> &saveTask;

  QStringList overlayTxtLines;

  float w;
  float h;
  int flags;

  KawaiiCamera* mainCamera;
  KawaiiCamera* fboCamera;  
  void createCameras();

  KawaiiMaterial *material;
  void createMaterial();

  KawaiiImage* textureImg;
  KawaiiGpuBuf *imgSender;
  void createTexture();

  KawaiiRenderpass *offscrRp0;
  KawaiiSceneLayer *offscrSc0;
  KawaiiFramebuffer* fbo0;
  KawaiiImage *textureFbo0;
  void createFbo0();

  KawaiiRenderpass *offscrRp1;
  KawaiiSceneLayer *offscrSc1;
  KawaiiFramebuffer* fbo1;
  KawaiiImage *textureFbo1;
  void createFbo1();


  KawaiiModel3D *torusModel;
  void createTorus();

  KawaiiModel3D *cubeModel;
  void createCube();

  KawaiiModel3D *rectanglesModel;
  void createRectangles();

  KawaiiProgram *shaderpack;
  void createShaderpack();

  KawaiiModel3D* createFigure(const QString &figure, const QMatrix4x4 &trMat, KawaiiGpuBuf *ubo);
  KawaiiMesh3D* createRectangle(const QVector3D &position = QVector3D());

  KawaiiGpuBuf* createGpuBuf(const UniformData &d, sib_utils::TreeNode *parent, const QLatin1String &objectName = QLatin1String());
  KawaiiGpuBuf* createGpuBuf(const UniformData &d, const QLatin1String &objectName = QLatin1String());
  KawaiiGpuBuf* createTexSender(KawaiiTexture *tex);

  void createGlobalUboBinding();

public:
  float frametime;
  float minFrametime;
  float maxFrametime;

  float rendertime;

  // QObject interface
protected:
  void timerEvent(QTimerEvent *event) override;
};

#endif // SIMPLESCENE_HPP
