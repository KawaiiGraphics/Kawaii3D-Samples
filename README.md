# Kawaii3D samples
Examples of using Kawaii3D graphics engine.
Developers of Kawaii3D can use this samples as integration tests of whole system (Kawaii3D + plugins + render).
Helps developers to learn Kawaii3D.
Helps users to check their Kawaii3D installation.

# SceneViewer
Renders all the scenes from command line arguments.
Scenes may be .mem files (sib\_utils::Memento memory chunks).
OBJ Wavefront, Open Collada, FBX and MD5 formats are supported by the KawaiiAssimp plugin and libassimp library.

# SceneGenerator
Creates and renders simple scene.
Saves memento memory chunk of this scene to 'share/Scenes/DefaultKawaiiScene.mem'.

